var viewer = new Cesium.Viewer('cesiumContainer', {timeline : false, animation : false});

//var $ = require("jquery");

//var viewer = new Cesium.Viewer('cesiumContainer');
    var Sport_Complex = viewer.entities.add({
    	name : 'Sport_Complex',
    	position : Cesium.Cartesian3.fromDegrees(100.772218,13.730026),
    	model : {
    		uri : '../../Apps/SampleData/models/KMITL/Sport_Complex.glb'
    	}
    });
    var Administration = viewer.entities.add({
    	name : 'Administration',
    	position : Cesium.Cartesian3.fromDegrees(100.777777,13.730801),
    	model : {
    		uri : '../../Apps/SampleData/models/KMITL/Administration.glb'
    	}
    });
    var Common_Lecture = viewer.entities.add({
    	name : 'Common_Lecture',
    	position : Cesium.Cartesian3.fromDegrees(100.776646,13.730064),
    	model : {
    		uri : '../../Apps/SampleData/models/KMITL/Common_Lecture.glb'
    	}
    });
	var Alumni  = viewer.entities.add({
		name : 'Alumni',     
		position : Cesium.Cartesian3.fromDegrees(100.774726,13.730995),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Alumni.glb'
		}
	});
	var International_College  = viewer.entities.add({
		name : 'International_College',     
		position : Cesium.Cartesian3.fromDegrees(100.775338,13.730008),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/International_College.glb'
		}
	});
	var Computer_Dept  = viewer.entities.add({
		name : 'Computer_Dept',     
		position : Cesium.Cartesian3.fromDegrees(100.775661,13.729053),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Computer_Dept.glb'
		}
	});
	var Student_Dormitory  = viewer.entities.add({
		name : 'Student_Dormitory',     
		position : Cesium.Cartesian3.fromDegrees(100.77423,13.729324),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Student_Dormitory.glb'
		}
	});
	var Nanotechnology  = viewer.entities.add({
		name : 'Nanotechnology',     
		position : Cesium.Cartesian3.fromDegrees(100.777446,13.729143),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Nanotechnology.glb'
		}
	});
	var Main_Auditiorium  = viewer.entities.add({
		name : 'Main_Auditiorium',     
		position : Cesium.Cartesian3.fromDegrees(100.777273,13.727453),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Main_Auditiorium.glb'
		}
	});
	var Fac_Of_Eng  = viewer.entities.add({
		name : 'Fac_Of_Eng',     
		position : Cesium.Cartesian3.fromDegrees(100.776389,13.727043),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Fac_Of_Eng.glb'
		}
	});
	var HM_Lecture  = viewer.entities.add({
		name : 'HM_Lecture',     
		position : Cesium.Cartesian3.fromDegrees(100.775264,13.72657),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/HM_Lecture.glb'
		}
	});
	var E12_Lecture  = viewer.entities.add({
		name : 'E12_Lecture',     
		position : Cesium.Cartesian3.fromDegrees(100.772438,13.727596),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/E12_Lecture.glb'
		}
	});
	var Arch_Office  = viewer.entities.add({
		name : 'Arch_Office',     
		position : Cesium.Cartesian3.fromDegrees(100.776365,13.724911),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Arch_Office.glb'
		}
	});
	var Arch_Auditiorium  = viewer.entities.add({
		name : 'Arch_Auditiorium',     
		position : Cesium.Cartesian3.fromDegrees(100.774986,13.725456),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Arch_Auditiorium.glb'
		}
	});
	var Arch_Building  = viewer.entities.add({
		name : 'Arch_Building',     
		position : Cesium.Cartesian3.fromDegrees(100.773693,13.725313),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Arch_Building.glb'
		}
	});
	var Arch_Building2  = viewer.entities.add({
		name : 'Arch_Building2',     
		position : Cesium.Cartesian3.fromDegrees(100.775277,13.72521),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Arch_Building2.glb'
		}
	});
	var Staff_Condo  = viewer.entities.add({
		name : 'Staff_Condo',     
		position : Cesium.Cartesian3.fromDegrees(100.771639,13.72543),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Staff_Condo.glb'
		}
	});
	var Agri_Agro  = viewer.entities.add({
		name : 'Agri_Agro',     
		position : Cesium.Cartesian3.fromDegrees(100.781289,13.726021),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Agri_Agro.glb'
		}
	});
	var Grand_Auditiorium  = viewer.entities.add({
		name : 'Grand_Auditiorium',     
		position : Cesium.Cartesian3.fromDegrees(100.779087,13.726529),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Grand_Auditiorium.glb'
		}
	});
	var Central_lib  = viewer.entities.add({
		name : 'Central_lib',     
		position : Cesium.Cartesian3.fromDegrees(100.778623,13.727569),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Central_lib.glb'
		}
	});
	var Tele_Dept  = viewer.entities.add({
		name : 'Tele_Dept',     
		position : Cesium.Cartesian3.fromDegrees(100.775826,13.727376),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Tele_Dept.glb'
		}
	});
	var Elec_Dept  = viewer.entities.add({
		name : 'Elec_Dept',     
		position : Cesium.Cartesian3.fromDegrees(100.775339,13.727414),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Elec_Dept.glb'
		}
	});
	var Inst_Dept  = viewer.entities.add({
		name : 'Inst_Dept',     
		position : Cesium.Cartesian3.fromDegrees(100.774245,13.72743),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Inst_Dept.glb'
		}
	});
	var Civil_Dept  = viewer.entities.add({
		name : 'Civil_Dept',     
		position : Cesium.Cartesian3.fromDegrees(100.773477,13.726653),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Civil_Dept.glb'
		}
	});
	var Plaza  = viewer.entities.add({
		name : 'Plaza',     
		position : Cesium.Cartesian3.fromDegrees(100.776893,13.729076),
		model : {
			uri : '../../Apps/SampleData/models/KMITL/Plaza.glb'
		}
	});
	var KMIDSM  = viewer.entities.add({
		name: 'KMIDS',
		id: 'Cupcake SVG',
	     position : Cesium.Cartesian3.fromDegrees(100.790682,13.731128),
	     model : {
	     	//uri : '../../../Specs/Data/Models/Box/CesiumBoxTest.gltf'
	     	uri : '../../Apps/SampleData/models/KMITL/KMIDS-M.glb'
	     }
    });
	var KMIDSB  = viewer.entities.add({
		name: 'KMIDS',
	     position : Cesium.Cartesian3.fromDegrees(100.790645,13.729028),
	     model : {
	     	uri : '../../Apps/SampleData/models/KMITL/KMIDS-B.glb'
	     }
	 });
	var KMIDSA  = viewer.entities.add({
		name: 'KMIDS',
	     position : Cesium.Cartesian3.fromDegrees(100.790581,13.729565),
	     model : {
	     	uri : '../../Apps/SampleData/models/KMITL/KMIDS-A.glb'
	     }
	 });
	var KMIDSC  = viewer.entities.add({
		name: 'KMIDS',
	     position : Cesium.Cartesian3.fromDegrees(100.790285,13.730615),
	     model : {
	     	uri : '../../Apps/SampleData/models/KMITL/KMIDS-C.glb'
	     }
	 });
	var KMIDSD  = viewer.entities.add({
		name: 'KMIDS',
	     position : Cesium.Cartesian3.fromDegrees(100.790913,13.730644),
	     model : {
	     	uri : '../../Apps/SampleData/models/KMITL/KMIDS-D.glb'
	     }
	 });


     viewer.zoomTo(KMIDSM);