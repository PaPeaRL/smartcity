//  var ModelMaterial = new Cesium.ModelMaterial();
//  function getModelForEntity(entity) {
//     var primitives = viewer.scene.primitives;
//     for (var i = 0; i < primitives.length; i++) {
//         var primitive = primitives.get(i);
//         if (primitive instanceof Cesium.Model && primitive.id === entity) {
//             return primitive;
//         }
//     }
// };

// var model = getModelForEntity(KMIDSM);
// if (model) {
//     var material = model.getMaterial('Red');
//     material.setValue('diffuse', Cesium.Cartesian4.fromColor(Cesium.Color.fromRandom()));
// }

//  setInterval(function(){  
//     model = getModelForEntity(KMIDSM);
//     if (model) {
//         material = model.getMaterial('Red');
//         console.log(material);
//         //material.setValue('diffuse', Cesium.Cartesian4.fromColor(Cesium.Color.fromRandom()));
//     }

//  console.log('color');
//  }, 3000);

// var viewer = new Cesium.Viewer('cesiumContainer', {
//     infoBox : false,
//     selectionIndicator : false
// });

// var entity = viewer.entities.add({
//     position : Cesium.Cartesian3.fromDegrees(100.790913,13.730644),
//     model : {
//         uri : '../../Apps/SampleData/models/KMITL/KMIDS-M.gltf',
//         minimumPixelSize : 128
//     }
// });
//  viewer.zoomTo(entity, new Cesium.HeadingPitchRange(Math.PI / 4, -Math.PI / 4, 3));

//Change color on mouse over.  This relies on the fact that given a primitive,
//you can retrieve an associted en
setInterval(function(){ 
var lastColor;
var lastPick;
var handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
handler.setInputAction(function(movement) {
    var primitive;
    var pickedObject = viewer.scene.pick(movement.endPosition);
    if (pickedObject) {
        primitive = pickedObject.primitive;
        if (pickedObject !== lastPick && primitive instanceof Cesium.Model) {

            //We don't use the entity here, but if you need to color based on 
            //some entity property, you can get to that data it here.
            var entity = primitive.id;
            
            var material = primitive.getMaterial('material');
            lastColor = material.getValue('diffuse').clone();
            material.setValue('diffuse', Cesium.Cartesian4.fromColor(Cesium.Color.LIGHTSKYBLUE));
            lastPick = pickedObject;
        }
    } else if (lastPick) {
        primitive = lastPick.primitive;
        var material = primitive.getMaterial('material');
        material.setValue('diffuse', Cesium.Cartesian4.fromColor(Cesium.Color.WHITE));
        lastPick = undefined;
    }
}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);

//Ability to look up the Model associated with an entity
function getModelForEntity(entity) {
    var primitives = viewer.scene.primitives;
    for (var i = 0; i < primitives.length; i++) {
        var primitive = primitives.get(i);
        if (primitive instanceof Cesium.Model && primitive.id === entity) {
            return primitive;
        }
    }
};
}, 3000);
//Makes an entity model a random color.
// Sandcastle.addToolbarButton('Make random color', function() {
//     var model = getModelForEntity(entity);
//     if (model) {
//         var material = model.getMaterial('Red');
//         material.setValue('diffuse', Cesium.Cartesian4.fromColor(Cesium.Color.fromRandom()));
//     }
// });