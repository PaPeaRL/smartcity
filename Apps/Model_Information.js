Sport_Complex.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="https://cie.kmitl.ac.th/wp-content/uploads/2016/01/IMG_2504-550x400.jpg"/>\
    <p>\
    Sport Complex Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - Gym<br/>\
    - Swimming Pool<br/>\
    - Running Track<br/>\
    - Stand<br/>\
    - Football Field<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Sport_Complex">Sport Complex</a>\
    </p>';
Administration.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://image.dek-d.com/25/1089566/110621912"/>\
      <p>\
    H.Q. (Rector and Central Administration) Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 10th <br/>\
    - Chancellor Office<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Administration">Administration</a>\
    </p>';
Common_Lecture.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://mw2.google.com/mw-panoramio/photos/medium/24719679.jpg"/>\
    <p>\
    Common Lecture Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - C-D Zone 5th Floor<br/>\
    - A-B Zone 3rd Floor<br/>\
    - New Education World<br/>\
    - Canteen<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Common_Lecture">Common_Lecture</a>\
    </p>';
Alumni.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.kmitlalumni.com/sites/default/files/about2.jpg"/>\
    <p>\
    Alumni Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - Restuarant<br/>\
    - 5th Floor<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Alumni">Alumni</a>\
    </p>';
International_College.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.fotorelax.com/forum/index.php?action=dlattach;topic=6829.0;attach=110377"/>\
    <p>\
    International College, Nanotechnology College,\
    Data College Innovation College Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="International_College">International_College</a>\
    </p>';
Computer_Dept.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://4.bp.blogspot.com/-9Y-semYvCjk/Vie3KP5Wd2I/AAAAAAAABFg/Hrv3uK4WdjM/s1600/ecc.jpg"/>\
    <p>\
    Department of Electrical, Computer, and Control Engineering Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 9th Floor<br/>\
    - 5th-9th Computer Engineering <br/>\
    - 1st-4th Electrical Engineering<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Computer_Dept">Computer_Dept</a>\
    </p>';
Student_Dormitory.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.kmitl.ac.th/stulife/images/dormitory-2.jpg"/>\
    <p>\
    Student Dormitory Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 6th Common Dormitory<br/>\
    - 4th Floor<br/>\
    - 2th Conditioner Dormitory<br/>\
    - 6th Floor<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Student_Dormitory">Student_Dormitory</a>\
    </p>';
Nanotechnology.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.manager.co.th/asp-bin/Image.aspx?ID=1325745"/>\
    <p>\
    Nanotechnology Research Center Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Nanotechnology">Nanotechnology</a>\
    </p>';
Main_Auditiorium.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://image.dek-d.com/18/1173068/15093515"/>\
    <p>\
    Main Auditorium Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - Contain 2500 person<br/>\
    - Slope<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Main_Auditiorium">Main_Auditiorium</a>\
    </p>';
Fac_Of_Eng.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://image.dek-d.com/25/1089566/110621377"/>\
    <p>\
    Faculty of Engieering Dean Office Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 5th Floor<br/>\
    - Dean Office<br/>\
    - A Building<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Fac_Of_Eng">Fac_Of_Eng</a>\
    </p>';
HM_Lecture.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="https://cie.kmitl.ac.th/wp-content/uploads/2016/01/IMG_5481-550x400.jpg"/>\
    <p>\
    HM Lecture Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 7th Floor<br/>\
    - Classroom<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="HM_Lecture">HM_Lecture</a>\
    </p>';
E12_Lecture.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://f.ptcdn.info/892/034/000/1440753623-72-o.jpg"/>\
    <p>\
    E12 Lecture Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 12th Floor<br/>\
    - Robot Club<br/>\
    - Slope Room Building<br/>\
    - True Lab<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="E12_Lecture">E12_Lecture</a>\
    </p>';
Arch_Office.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.bloggang.com/data/a/arjae/picture/1267631954.jpg"/>\
    <p>\
    Faculty of Architecture Office Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Arch_Office">Arch_Office</a>\
    </p>';
Arch_Auditiorium.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://image.dek-d.com/25/1089566/110621775"/>\
    <p>\
    Architecture Auditorium Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Arch_Auditiorium">Arch_Auditiorium</a>\
    </p>';
Arch_Building.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.arch.kmitl.ac.th/new/th/images/main/submenu/thumbnail/2.jpg"/>\
    <p>\
    Architecture Lecture Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Arch_Building">Arch_Building</a>\
    </p>';
Arch_Building2.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.suanboard.net/userimage/34132_227102.jpg"/>\
    <p>\
    Faculty of Architecture Department Building Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Arch_Building2">Arch_Building2</a>\
    </p>';
Staff_Condo.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.thairoomonline.com/images/items/sw_mansion-01.jpg"/>\
    <p>\
    Staff Condominium Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 10th Floor<br/>\
    - Resident of Professor and staff<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Staff_Condo">Staff_Condo</a>\
    </p>';
Agri_Agro.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.agri.kmitl.ac.th/Pic/5.jpg"/>\
    <p>\
    Faculty of Agricultural Technology\
Faculty of Agro-Industry Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Agri_Agro">Agri_Agro</a>\
    </p>';
Grand_Auditiorium.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.cicpaint.com/gallery/7799765-5f018d15851a45e0c16106a3fd25491e_dsc00020-1.jpg"/>\
    <p>\
    Grand Auditorium Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - Contain Five Thousand People<br/>\
    - Exhibition PLace<br/>\
    - Saminar Room<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Grand_Auditiorium">Grand_Auditiorium</a>\
    </p>';
Central_lib.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://www.pen1.biz/newwww/images/8%20kmitl2.jpg"/>\
    <p>\
    Central Library Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - 6th Floor<br/>\
    - Coffee Shop<br/>\
    - Meeting Room<br/>\
    - Theater<br/>\
    - Computer Room<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Central_lib">Central_lib</a>\
    </p>';
Tele_Dept.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://engineer.kmitl.ac.th/engineer2015/wp-content/uploads/2015/02/engineer2503-telecomunication.jpg"/>\
    <p>\
    Department of Telecommunication Engineering Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Tele_Dept">Tele_Dept</a>\
    </p>';
Elec_Dept.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://161.246.35.211/wp-content/uploads/2015/01/elec1.jpg"/>\
    <p>\
    Department of Electronics Engineering Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Elec_Dept">Elec_Dept</a>\
    </p>';
Inst_Dept.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="http://instrumentation.kmitl.ac.th/ins/wp-content/uploads/2013/08/970938_496480790439591_874287818_n.jpg"/>\
    <p>\
    Department of Instrumentation Engineering Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Inst_Dept">Inst_Dept</a>\
    </p>';
Civil_Dept.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="https://scontent.fbkk1-1.fna.fbcdn.net/v/t1.0-9/11011108_738894079560572_2145786550112743221_n.jpg?oh=156f03ff822ea1c609c3ecec1f3a4521&oe=5828960A"/>\
    <p>\
    Department of Civil Engineering Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Civil_Dept">Civil_Dept</a>\
    </p>';
Plaza.description = '\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0";\
      src="https://irs3.4sqi.net/img/general/width960/6356905_VgC5H93e--DIWH4Tt7eRohEalUsAoj8sdCrvPfWYLWA.jpg"/>\
    <p>\
    Plaza Avenue Of King Mongkut’s Institute of Technology Ladkrabang<br/>\
    - Book Store<br/>\
    - Krungthai Bank<br/>\
    - Krungsri Bank<br/>\
    - Kasikorn Bank<br/>\
    - Ture Coffee<br/>\
    - Post Office<br/>\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="Plaza">Plaza</a>\
    </p>';
    KMIDSM.description = KMIDSA.description = KMIDSB.description =  KMIDSC.description = KMIDSD.description ='\
    <img\
      width="50%"\
      style="float:left; margin: 0 1em 1em 0;"\
      src="https://scontent.fbkk5-6.fna.fbcdn.net/v/t1.0-9/13912384_1185669311454205_1580243059135299002_n.jpg?oh=018ee354bb30a6f84d07ad09433592cf&oe=58116870"/>\
    <p>\
    King Mongkut’s International Demonstration School<br/>\
    THAILAND’S first international science demonstration school is springing into action.<br/>\
    The opening is a part of efforts by\
    King Mongkuts Institute of Technology Lat Krabang (KMITL)\
    to provide an incubator for students\
    so that they can become innovators in the 21st Century.\
    </p>\
    <p>\
    King Mongkut’s Institute of Technology Ladkrabang\
    Chalongkrung Rd.\
    Ladkrabang , Bangkok Thailand\
    Postal code : 10520\
    Phone number +66(0) 2329 8000 - 2329 8099\
    </p>\
      Source: \
      <a style="color: WHITE"\
        target="_blank"\
        href="KMIDS>KMIDS</a>\
    </p>';
